export const validateAmount = value => {
	let error;
	if (!value) {
		error = 'Required';
	} else if (value.length < 1) {
		error = 'Must be 1 characters or more';
	} else if (value <= 0) {
		error = 'Must be more then 0';
	}
	return error;
};

export const validateRequire = value => {
	let error;
	if (!value) {
		error = 'Required';
		return error;
	}
};
