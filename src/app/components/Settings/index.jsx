import React, { Component } from 'react';
import { Field, reduxForm, formValueSelector, initialize } from 'redux-form';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Select from 'app/components/UI/Select';
import Input from 'app/components/UI/Input';
import { validateRequire } from 'app/helpers/validations.js';

import {
	selectors as statsSelectors,
	newCategory,
	deleteTransactionsCategoryByName
} from 'app/ducks/stats.js';
import style from './style.scss';

class Settings extends Component {
	handleFormSubmit = values => {
		const { dispatch } = this.props;
		dispatch(initialize('EditCategory', { Category: values.NewCategory }));
	};

	handleAddNewCategory = () => {
		const { dispatch, newCategoryName } = this.props;
		if (newCategoryName) {
			dispatch(newCategory(newCategoryName));
		}
	};

	handleDeleteCategory = () => {
		const { dispatch, category } = this.props;
		if (category) dispatch(deleteTransactionsCategoryByName(category));
	};

	render() {
		const { category, handleSubmit, transactionsCategorys } = this.props;
		console.log('category', category);
		return (
			<div className={style.settingsWrap}>
				<form
					className={style.editCategorysWrap}
					onSubmit={handleSubmit(this.handleFormSubmit)}
				>
					<div className={style.row}>
						<Field
							name="Category"
							component={Select}
							type="text"
							text="Choose category: "
							category={true}
							elements={transactionsCategorys}
						/>
						{category !== 'Add new category' && category ? (
							<div className={style.buttonWrap}>
								<button
									className={style.dellCategoryButton}
									onClick={this.handleDeleteCategory}
								>
									Delete Category
								</button>
							</div>
						) : null}
					</div>
					<div className={style.row}>
						{category === 'Add new category' && (
							<div className={style.newCategoryWrap}>
								<Field
									name="NewCategory"
									component={Input}
									type="text"
									text="New category name: "
									elements={transactionsCategorys}
									validate={validateRequire}
								/>
								<div className={style.buttonWrap}>
									<button
										className={style.addCategoryButton}
										onClick={this.handleAddNewCategory}
									>
										Add Category
									</button>
								</div>
							</div>
						)}
					</div>
				</form>
			</div>
		);
	}
}

const selector = formValueSelector('EditCategory');

const mapStateToProps = state => {
	return {
		category: selector(state, 'Category')
		, newCategoryName: selector(state, 'NewCategory')
		, transactionTypes: statsSelectors.getTransactionTypes(state)
		, transactionsCategorys: statsSelectors.getTransactionsCategorys(state)
		, transactionsList: statsSelectors.getTransactionsList(state)
	};
};

Settings.propTypes = {
	dispatch: PropTypes.func
	, handleSubmit: PropTypes.func
	, closeTransactionField: PropTypes.func
	, category: PropTypes.string
	, transactionTypes: PropTypes.array
	, transactionsList: PropTypes.array
	, transactionsCategorys: PropTypes.array
	, newCategoryName: PropTypes.string
	, transactionIndexForEdit: PropTypes.number
};

export default connect(mapStateToProps)(
	reduxForm({ form: 'EditCategory' })(Settings)
);
