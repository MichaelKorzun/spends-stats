import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
	selectors as statsSelectors,
	deleteTransactionByIndex
} from 'app/ducks/stats';
import Controls from 'app/components/Controls';
import Transaction from 'app/components/Transaction';
import style from './style.scss';

class Transactions extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showAddTransactionField: false
			, transactionIndexForEdit: -1
		};
	}

	handleDeleteTransaction = (_, transactionIndex) => {
		const { dispatch } = this.props;
		dispatch(deleteTransactionByIndex(transactionIndex));
	};

	handleEditTransaction = (_, transactionIndex) => {
		this.setState({
			transactionIndexForEdit: transactionIndex
			, showAddTransactionField: true
		});
	};

	transactionField = () => {
		this.setState(prevState => ({
			transactionIndexForEdit: -1
			, showAddTransactionField: !prevState.showAddTransactionField
		}));
	};

	render() {
		const { showAddTransactionField, transactionIndexForEdit } = this.state;
		const { transactionsList } = this.props;
		return (
			<div className={style.transactions}>
				<Controls addTransaction={this.transactionField} />
				{showAddTransactionField && (
					<Transaction
						closeTransactionField={this.transactionField}
						transactionIndexForEdit={transactionIndexForEdit}
					/>
				)}
				<ul className={style.transactionsList}>
					<li className={`${style.transaction} ${style.transactionTitle}`}>
						<span>Type</span>
						<span>Category</span>
						<span>Date</span>
						<span>Note</span>
						<span>Amount</span>
					</li>
					{transactionsList.length
						? transactionsList.map((item, index) => {
							return (
								<li
									className={`${style.transaction} ${style.transactionData}`}
									key={index}
								>
									<span>{item.Type}</span>
									<span>{item.Category}</span>
									<span>{item.Date}</span>
									<span>{item.Note}</span>
									<span>{item.Amount}</span>
									<div className={style.buttonsWrap}>
										<button
											className={style.edit}
											onClick={e => this.handleEditTransaction(e, index)}
										/>
										<button
											className={style.delete}
											onClick={e => this.handleDeleteTransaction(e, index)}
										/>
									</div>
								</li>
							);
						})
						: null}
				</ul>
			</div>
		);
	}
}

Transactions.propTypes = {
	dispatch: PropTypes.func
	, transactionsList: PropTypes.array
};

const mapStateToProps = state => {
	return {
		transactionsList: statsSelectors.getTransactionsList(state)
	};
};

export default connect(mapStateToProps)(Transactions);
