import React, { Component } from 'react';
import { NavLink, Link } from 'react-router-dom';
import style from './style.scss';
import logo from 'app/assets/img/logo.png';

class NavMenu extends Component {
	render() {
		return (
			<div className={style.navMenu}>
				<div className={style.navMenuWrap}>
					<Link to="/" className={style.logoWrap}>
						<img className={style.logo} src={logo} />{' '}
					</Link>
					<NavLink to="/transactions" activeClassName={style.active}>
						Spends list
					</NavLink>
					<NavLink to="/charts" activeClassName={style.active}>
						Spends diagrams
					</NavLink>
					<NavLink to="/settings" activeClassName={style.active}>
						My settings
					</NavLink>
				</div>
			</div>
		);
	}
}
export default NavMenu;
