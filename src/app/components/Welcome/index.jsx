import React, { Component } from 'react';
import { withRouter } from 'react-router';
import style from './style.scss';

class Welcome extends Component {
	componentDidMount() {
		// this.props.history.push('/transactions');
	}
	render() {
		return (
			<div className={style.welcomeWrap}>
				<div className={style.texWrap}>
					<h2>Dear Sirs, Welcome to my Test project.</h2>
					<p>
						It was a pleasure developing this assignment as I had an opportunity
						to implement various technologies. I was trying to create clean and
						optimized code and beautiful interface.
					</p>
					<p>
						Thank you for giving me this captivating test project. I am passionate
						about the position you offer and will be pleased to tell you more
						about my technology stack during an interview.
					</p>
					<p>Looking forward to hear from you soon.</p>
					<p>Kind regards, Michael.</p>
				</div>
			</div>
		);
	}
}
export default withRouter(Welcome);
