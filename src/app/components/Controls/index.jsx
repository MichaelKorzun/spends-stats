import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import DateRangeModule from 'app/components/UI/DateRangeModule';
import style from './style.scss';

class Controls extends PureComponent {
	render() {
		const { addTransaction } = this.props;
		return (
			<div className={style.controls}>
				<button
					className={style.addTransactionButton}
					onClick={() => addTransaction()}
				>
					Add Transaction
				</button>
				<DateRangeModule />
			</div>
		);
	}
}
Controls.propTypes = {
	addTransaction: PropTypes.func
};
export default Controls;
