import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm, formValueSelector, initialize } from 'redux-form';
import { connect } from 'react-redux';
import DatePickerModule from 'app/components/UI/DatePickerModule';
import moment from 'moment';
import Input from 'app/components/UI/Input';
import Select from 'app/components/UI/Select';
import { validateAmount, validateRequire } from 'app/helpers/validations.js';
import {
	selectors as statsSelectors,
	newTransaction,
	newCategory,
	editTransactionsByIndex
} from 'app/ducks/stats.js';
import style from './style.scss';

const formInitialValues = {
	Type: ''
	, Date: moment(new Date()).format('YYYY-MM-DD')
	, Category: ''
	, Note: ''
	, Amount: ''
};

class Transaction extends Component {

	handleFormSubmit = values => {
		const { dispatch, transactionIndexForEdit } = this.props;
		console.log(transactionIndexForEdit);
		if (transactionIndexForEdit < 0) {
			dispatch(newTransaction(values));
			dispatch(initialize('Transaction', formInitialValues));
		} else {
			dispatch(
				editTransactionsByIndex({
					transaction: values
					, index: transactionIndexForEdit
				})
			);
		}
	};

	handleAddNewCategory = () => {
		const { dispatch, newCategoryName } = this.props;
		if (newCategoryName) {
			dispatch(newCategory(newCategoryName));
			dispatch(initialize('Transaction', formInitialValues));
		}
	};

	componentDidMount() {
		const { dispatch, transactionIndexForEdit, transactionsList } = this.props;
		dispatch(
			initialize(
				'Transaction',
				transactionIndexForEdit >= 0
					? transactionsList[transactionIndexForEdit]
					: formInitialValues
			)
		);
	}

	componentWillReceiveProps(nextProps) {
		const { transactionIndexForEdit, transactionsList, dispatch } = this.props;
		if (transactionIndexForEdit !== nextProps.transactionIndexForEdit) {
			dispatch(
				initialize(
					'Transaction',
					transactionsList[nextProps.transactionIndexForEdit]
				)
			);
		}
	}

	render() {
		const {
			closeTransactionField,
			category,
			handleSubmit,
			transactionTypes,
			transactionsCategorys,
			transactionIndexForEdit
		} = this.props;
		return (
			<div className={style.transaction}>
				<form
					className={style.transactionForm}
					onSubmit={handleSubmit(this.handleFormSubmit)}
				>
					<div className={style.row}>
						{transactionTypes && (
							<Field
								name="Type"
								component={Select}
								text="Choose type of transaction: "
								elements={transactionTypes}
								validate={validateRequire}
							/>
						)}
						{transactionsCategorys && (
							<Field
								name="Category"
								component={Select}
								type="text"
								text="Choose category: "
								category={true}
								elements={transactionsCategorys}
							/>
						)}
					</div>
					<div className={style.row}>
						{category === 'Add new category' && (
							<div className={style.newCategoryWrap}>
								<Field
									name="NewCategory"
									component={Input}
									type="text"
									text="New category name: "
									elements={transactionsCategorys}
									validate={validateRequire}
								/>
								<div className={style.buttonWrap}>
									<button
										className={style.addCategoryButton}
										onClick={this.handleAddNewCategory}
									>
										Add Category
									</button>
								</div>
							</div>
						)}
					</div>
					<div className={style.row}>
						<Field
							name="Date"
							component={DatePickerModule}
							type="text"
							text="Please chouse transaction date: "
							selected={moment()}
						/>
						<Field
							name="Note"
							component={Input}
							type="text"
							text="Add some note"
						/>
					</div>
					<div className={style.row}>
						<Field
							name="Amount"
							component={Input}
							type="number"
							text="Amount: "
							validate={validateAmount}
						/>
						<div className={style.buttonWrap}>
							<button className={style.addTransactionButton} type="submit">
								{transactionIndexForEdit < 0
									? 'Add Transaction'
									: 'Edit Transaction'}
							</button>
						</div>
					</div>
				</form>
				<button
					className={style.closeTransactionField}
					onClick={() => closeTransactionField()}
				>
					{' '}
					X{' '}
				</button>
			</div>
		);
	}
}

Transaction.propTypes = {
	dispatch: PropTypes.func
	, handleSubmit: PropTypes.func
	, closeTransactionField: PropTypes.func
	, category: PropTypes.string
	, transactionTypes: PropTypes.array
	, transactionsList: PropTypes.array
	, transactionsCategorys: PropTypes.array
	, newCategoryName: PropTypes.string
	, transactionIndexForEdit: PropTypes.number
};

const selector = formValueSelector('Transaction');

const mapStateToProps = state => {
	return {
		category: selector(state, 'Category')
		, newCategoryName: selector(state, 'NewCategory')
		, transactionTypes: statsSelectors.getTransactionTypes(state)
		, transactionsCategorys: statsSelectors.getTransactionsCategorys(state)
		, transactionsList: statsSelectors.getTransactionsList(state)
	};
};

export default connect(mapStateToProps)(
	reduxForm({ form: 'Transaction' })(Transaction)
);
