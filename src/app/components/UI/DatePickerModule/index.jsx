import React, { Component } from 'react';
import PropTypes from 'prop-types';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';
import style from './style.scss';

class DatePickerModule extends Component {

	handleChange = date => {
		this.setState({
			pickerDate: date
		});
		this.props.input.onChange(moment(date).format('YYYY-MM-DD'));
	};

	render() {
		const { text, input } = this.props;
		return (
			<div className={style.datePickerModule}>
				<p className={style.description}>{text}</p>
				<DatePicker
					{...input}
					onChange={this.handleChange}
				/>
			</div>
		);
	}
}

DatePickerModule.propTypes = {
	text: PropTypes.string
	, input: PropTypes.object
};

export default DatePickerModule;
