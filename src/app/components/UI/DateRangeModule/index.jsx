import React, { Component } from 'react';
import PropTypes from 'prop-types'; //eslint-disable-line
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';
import style from './style.scss';

class DateRangeModule extends Component {
	constructor(props) {
		super(props);
		this.state = {
			startDate: moment()
			, endDate: moment()
			, error: false
		};
	}

	handleChangeStart = date => {
		this.setState({
			startDate: date
			, error: true
		});
	};
	handleChangeEnd = date => {
		this.setState({
			endDate: date
			, error: true
		});
	};

	render() {
		const { error } = this.state;
		return (
			<div className={style.dateRangeModuleWrap}>
				{error && <span>Sorry Not working</span>}
				<div className={style.dateRangeModule}>
					<DatePicker
						selected={this.state.startDate}
						selectsStart
						startDate={this.state.startDate}
						endDate={this.state.endDate}
						onChange={this.handleChangeStart}
					/>
				</div>
				<div className={style.dateRangeModule}>
					<DatePicker
						selected={this.state.endDate}
						selectsEnd
						startDate={this.state.startDate}
						endDate={this.state.endDate}
						onChange={this.handleChangeEnd}
					/>
				</div>
			</div>
		);
	}
}

DateRangeModule.propTypes = {};

export default DateRangeModule;
