import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { selectors as statsSelectors } from 'app/ducks/stats';
import BarChart from './BarChart';
import LinerChart from './LinerChart';
import PieChartContainer from './PieChartContainer';

class Charts extends Component {
	render() {
		const { transactionsBalance, transactionsListRange, transactionsBtCategory } = this.props;
		return (
			<div>
				<BarChart transactionsListRange={transactionsListRange} />
				<LinerChart transactionsBalance={transactionsBalance} />
				<PieChartContainer transactionsBtCategory={transactionsBtCategory} />
			</div>
		);
	}
}

Charts.propTypes = {
	transactionsBalance: PropTypes.array
	, transactionsListRange: PropTypes.array
	, transactionsBtCategory: PropTypes.array
};

const mapStateToProps = state => {
	return {
		transactionsListRange: statsSelectors.getTransactionsListRange(state)
		, transactionsBalance: statsSelectors.getTransactionsBalance(state)
		, transactionsBtCategory: statsSelectors.getTransactionsCategoryList(state)
	};
};

export default connect(mapStateToProps)(Charts);
