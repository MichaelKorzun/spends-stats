import React from 'react';
import PropTypes from 'prop-types';
import {
	LineChart,
	Line,
	XAxis,
	YAxis,
	CartesianGrid,
	Tooltip,
	Legend
} from 'Recharts';
import style from './style.scss';

const LinerChart = props => {
	const { transactionsBalance } = props;
	return (
		<div className={style.linerChartWrap}>
			<LineChart
				width={1000}
				height={300}
				data={transactionsBalance}
				margin={{ top: 25, right: 5, left: 5, bottom: 5 }}
			>
				<XAxis dataKey="name" />
				<YAxis />
				<CartesianGrid strokeDasharray="3 3" />
				<Tooltip />
				<Legend />
				<Line
					type="monotone"
					dataKey="balance"
					stroke="#12c48b"
					activeDot={{ r: 8 }}
				/>
			</LineChart>
		</div>
	);
};
LinerChart.propTypes = {
	transactionsBalance: PropTypes.array
};
export default LinerChart;
