import React from 'react';
import PropTypes from 'prop-types';
import {
	BarChart,
	Bar,
	XAxis,
	YAxis,
	CartesianGrid,
	Tooltip,
	Legend
} from 'Recharts';
import style from './style.scss';

const LinerChart = props => {
	const { transactionsListRange } = props;
	return (
		<div className={style.barChratWrap}>
			<BarChart
				width={1000}
				height={300}
				data={transactionsListRange}
				margin={{ top: 25, right: 5, left: 5, bottom: 5 }}
			>
				<CartesianGrid strokeDasharray="3 3" />
				<XAxis dataKey="name" />
				<YAxis />
				<Tooltip />
				<Legend />
				<Bar dataKey="Income" fill="#12c48b" />
				<Bar dataKey="Expenses" fill="#f51c1c" />
			</BarChart>
		</div>
	);
};
LinerChart.propTypes = {
	transactionsListRange: PropTypes.array
};

export default LinerChart;
