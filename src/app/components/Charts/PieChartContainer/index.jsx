import React, { Component } from 'react';
import { PieChart, Pie, Cell } from 'Recharts';
import PropTypes from 'prop-types';
import style from './style.scss';

const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];

class PieChartContainer extends Component {
	render() {
		const { transactionsBtCategory } = this.props;
		return (
			<div className={style.pieChratWrap}>
				<PieChart width={900} height={600} onMouseEnter={this.onPieEnter}>
					<Pie
						data={transactionsBtCategory}
						cx={500}
						cy={300}
						labelLine={true}
						dataKey="value"
						label
						outerRadius={200}
						fill="#8884d8"
					>
						{transactionsBtCategory &&
							transactionsBtCategory.map((entry, index) => (
								<Cell key={index} fill={COLORS[index % COLORS.length]} />
							))}
					</Pie>
				</PieChart>
			</div>
		);
	}
}
PieChartContainer.propTypes = {
	transactionsBtCategory: PropTypes.array
};
export default PieChartContainer;
