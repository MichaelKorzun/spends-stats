import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
	selectors as statsSelectors,
	setInitialStateFromLS
} from 'app/ducks/stats';
import Transactions from 'app/components/Transactions';
import NavMenu from 'app/components/NavMenu';
import Charts from 'app/components/Charts';
import Settings from 'app/components/Settings';
import Welcome from 'app/components/Welcome';

//MOC
const mocData = {
	transactions: [
		{
			Type: 'Income'
			, Category: ''
			, Date: '2018-05-27'
			, Note: 'Income'
			, Amount: '4000'
		}
		, {
			Type: 'Income'
			, Category: ''
			, Date: '2018-05-27'
			, Note: 'Income2'
			, Amount: '5000'
		}
		, {
			Type: 'Expenses'
			, Category: 'Food'
			, Note: 'Food'
			, Amount: '400'
			, Date: '2018-05-15'
		}
		, {
			Type: 'Expenses'
			, Category: 'Drinks'
			, Note: 'Drinks'
			, Amount: '900'
			, Date: '2018-05-16'
		}
		, {
			Type: 'Expenses'
			, Category: 'Home'
			, Note: 'Home'
			, Amount: '20'
			, Date: '2018-05-18'
		}
		, {
			Type: 'Expenses'
			, Category: 'Wife'
			, Note: 'Wife'
			, Amount: '8000'
			, Date: '2018-05-01'
		}
	]
	, categorys: ['Food', 'Drinks', 'Home', 'Wife']
};

class App extends React.Component {
	componentDidMount() {
		const { dispatch } = this.props;
		const dataFromLS = localStorage.getItem('initialState');
		if (dataFromLS) {
			dispatch(setInitialStateFromLS(JSON.parse(dataFromLS)));
		} else {
			localStorage.setItem('initialState', JSON.stringify(mocData));
			dispatch(setInitialStateFromLS(mocData));
		}
	}

	componentWillReceiveProps(nextProps) {
		if (
			nextProps.transactionsList !== this.props.transactionsList ||
			nextProps.transactionsCategoryList !== this.props.transactionsCategoryList
		) {
			localStorage.clear();
			localStorage.setItem(
				'initialState',
				JSON.stringify({
					transactions: nextProps.transactionsList
					, categorys: nextProps.transactionsCategoryList
				})
			);
		}
	}

	render() {
		return (
			<BrowserRouter>
				<React.Fragment>
					<NavMenu />
					<Switch>
						<Route exact path="/" component={() => <Welcome {...this.props} />} />
						<Route exact path="/transactions" component={Transactions} />
						<Route exact path="/charts" component={Charts} />
						<Route exact path="/settings" component={Settings} />
					</Switch>
				</React.Fragment>
			</BrowserRouter>
		);
	}
}

App.propTypes = {
	dispatch: PropTypes.func
	, transactionsCategoryList: PropTypes.array
	, transactionsList: PropTypes.array
};

const mapStateToProps = state => {
	return {
		transactionsCategoryList: statsSelectors.getTransactionsCategorys(state)
		, transactionsList: statsSelectors.getTransactionsList(state)
	};
};

export default connect(mapStateToProps)(App);
