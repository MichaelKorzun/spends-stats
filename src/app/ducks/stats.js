export const ADD_NEW_TRANSACTION = 'ADD_NEW_TRANSACTION';
export const ADD_TRANSACTIONS_LIST = 'ADD_TRANSACTIONS_LIST';
export const ADD_CATEGORYS_LIST = 'ADD_CATEGORYS_LIST';
export const ADD_NEW_TRANSACTION_CATEGORY = 'ADD_NEW_TRANSACTION_CATEGORY';
export const DELETE_TRANSACTION = 'DELETE_TRANSACTION';
export const EDIT_TRANSACTION = 'EDIT_TRANSACTION';
export const DELETE_TRANSACTIONS_CATEGORY = 'DELETE_TRANSACTIONS_CATEGORY';

const initialState = {
	transactionTypes: ['Income', 'Expenses']
	, transactionsCategorys: []
	, transactionsList: []
};

export default (state = initialState, { type, payload }) => {
	switch (type) {
		case ADD_NEW_TRANSACTION:
			return {
				...state
				, transactionsList: [...state.transactionsList, payload]
			};

		case ADD_TRANSACTIONS_LIST:
			return {
				...state
				, transactionsList: payload
			};

		case ADD_CATEGORYS_LIST:
			return {
				...state
				, transactionsCategorys: payload
			};

		case DELETE_TRANSACTION:
			return {
				...state
				, transactionsList: state.transactionsList.filter(
					(item, index) => index !== payload
				)
			};

		case DELETE_TRANSACTIONS_CATEGORY:
			return {
				...state
				, transactionsCategorys: state.transactionsCategorys.filter(
					item => item !== payload
				)
			};

		case ADD_NEW_TRANSACTION_CATEGORY: {
			return {
				...state
				, transactionsCategorys: [...state.transactionsCategorys, payload]
			};
		}

		case EDIT_TRANSACTION: {
			return {
				...state
				, transactionsList: [
					...state.transactionsList.slice(0, payload.index)
					, payload.transaction
					, ...state.transactionsList.slice(payload.index + 1)
				]
			};
		}

		default:
			return state;
	}
};

export const addNewTransaction = payload => ({
	type: ADD_NEW_TRANSACTION
	, payload
});

export const addTransactionsList = payload => ({
	type: ADD_TRANSACTIONS_LIST
	, payload
});

export const addCategorysList = payload => ({
	type: ADD_CATEGORYS_LIST
	, payload
});

export const addNewCategory = payload => ({
	type: ADD_NEW_TRANSACTION_CATEGORY
	, payload
});

export const deleteTransaction = payload => ({
	type: DELETE_TRANSACTION
	, payload
});

export const deleteTransactionsCategory = payload => ({
	type: DELETE_TRANSACTIONS_CATEGORY
	, payload
});

export const editTransaction = payload => ({
	type: EDIT_TRANSACTION
	, payload
});

export const setInitialStateFromLS = initialState => dispatch => {
	dispatch(addTransactionsList(initialState.transactions));
	dispatch(addCategorysList(initialState.categorys));
};

export const newTransaction = transaction => dispatch => {
	dispatch(addNewTransaction(transaction));
};

export const newCategory = category => dispatch => {
	dispatch(addNewCategory(category));
};

export const deleteTransactionByIndex = transactionIndex => dispatch => {
	dispatch(deleteTransaction(transactionIndex));
};

export const deleteTransactionsCategoryByName = category => dispatch => {
	dispatch(deleteTransactionsCategory(category));
};

export const editTransactionsByIndex = transactionData => dispatch => {
	dispatch(editTransaction(transactionData));
};

export const selectors = {
	getTransactionTypes: state => state.stats.transactionTypes
	, getTransactionsCategorys: state => state.stats.transactionsCategorys
	, getTransactionsList: state =>
		state.stats.transactionsList.sort(
			(a, b) => new Date(a.Date) - new Date(b.Date) || a.Amount - b.Amount
		)
	, getTransactionsBalance: state => {
		let balance = 0;
		const newArray = [];
		state.stats.transactionsList
			.sort((a, b) => new Date(a.Date) - new Date(b.Date))
			.forEach(transaction => {
				const rawObject = {};
				if (transaction.Type === 'Income') balance = +transaction.Amount;
				if (transaction.Type === 'Expenses') balance = -+transaction.Amount;
				rawObject.name = transaction.Date;
				rawObject.balance = balance;
				newArray.push(rawObject);
			});
		return newArray;
	}
	, getTransactionsListRange: state => {
		const newArray = [];
		state.stats.transactionsList
			.sort((a, b) => new Date(a.Date) - new Date(b.Date))
			.forEach(transaction => {
				const rawObject = {};
				rawObject.name = transaction.Date;
				if (transaction.Type === 'Income')
					(rawObject.Income = +transaction.Amount), (rawObject.Expenses = 0);
				if (transaction.Type === 'Expenses')
					(rawObject.Expenses = -+transaction.Amount), (rawObject.Income = 0);
				newArray.push(rawObject);
			});
		return newArray;
	}
	, getTransactionsCategoryList: state => {
		const newArray = [];
		state.stats.transactionsCategorys.forEach(category => {
			const catSum = state.stats.transactionsList.reduce((acc, transaction) => {
				let tmp = 0;
				if (
					transaction.Type === 'Expenses' &&
					transaction.Category === category
				) {
					tmp = +transaction.Amount;
				}
				return acc + tmp;
			}, 0);
			const rawObject = {};
			rawObject.name = category;
			rawObject.value = catSum;
			rawObject.value && newArray.push(rawObject);
		});
		return newArray;
	}
};
