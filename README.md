# My spends stats app
# clone repository
git clone git@bitbucket.org:MichaelKorzun/spends-stats.git

# install dependencies
npm install

# serve at http://localhost:4000/
npm run devstart

# build for production
npm run build

#deployed version at https://michaelkorzuntesttaskspendstat.herokuapp.com/