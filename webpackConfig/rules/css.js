const cssLoader = require('../loaders/css');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = ({ include, isProd, sourcePath }) => {
	return ({
		test: /\.(scss|sass|css)$/
		, include
		, use: ExtractTextPlugin.extract({
			fallback: 'style-loader'
			, use: cssLoader({ isProd, sourcePath })
		})
	});
};
