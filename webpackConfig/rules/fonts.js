module.exports = () => {
	return ({
		test: /\.(eot|svg|ttf|woff2?)$/
		, use: {
			loader: 'file-loader'
			, options: {
				name: 'static/[name]-[hash:8].[ext]'
			}
		}
	});
};
